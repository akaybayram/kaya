# Kaya

![Kütüphane Logosu](kaya_logo.png "Kütüphane Logosu (Dall-E ile üretildi.")

Kaya C ile program geliştirmeyi hızlandırmayı ve kolaylaştırmayı amaçlayan bir kütüphanedir. Sadece ".h" uzantılı başlık dosyasından oluşan header-only olarak isimlendirilen bir teknik ile yazılmıştır.

Normal olarak include edildiğinde başlık dosyası olarak davranırken (sadece fonksiyon beyanlarını içerir tanımlar bulunmaz) include edilmeden önce `#define KAYA_TANIMLAR` tanımlaması yapılarak fonksiyonların tanımları da eklenebilir. 

```c
#define KAYA_TANIMLAR
#include "kaya/kaya.h"
```

Bu işlem sık değiştirilmeyen kaynak dosyalarından birinde yapılmalıdır. Birden fazla kaynak dosyasında yapılması halinde "multiple definition" hatası alınacaktır.

Program dağıtım için derlenmek istendiğinde geliştirme zamanı kolaylık olması açısından eklenen kontroller `#define KAYA_DAGITIM_VERSIYONU` tanımı yapılarak devre dışı bırakılabilir.

```c
#define KAYA_DAGITIM_VERSIYONU
#define KAYA_TANIMLAR
#include "kaya/kaya.h"
```

Planlanan yenilikler;
1. kod içi ve harici dökümantasyon,
2. `kaya_komut_calistir` geliştirmeleri,
3. komut satırı argümanları yönetimi

#ifndef KAYA_LIB_H
#define KAYA_LIB_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/stat.h>

#ifdef _WIN32
#include <process.h>
#include <io.h>
#include <direct.h>
#include <synchapi.h>

#define _execv(prog, arg_list) execv(prog, arg_list)
#define _access(file, mode) access(file, mode)
#define mkdir(dir, mode) _mkdir(dir)
#define open(name, ...) _open(name, __VA_ARGS__)
#define read(fd, buf, count) _read(fd, buf, count)
#define close(fd) _close(fd)
#define write(fd, buf, count) _write(fd, buf, count)
#define dup2(fd1, fd2) _dup2(fd1, fd2)
#define unlink(file) _unlink(file)
#define rmdir(dir) _rmdir(dir)
#define getpid() _getpid()
#define usleep(t) Sleep((t)/1000)
#define sleep(t) Sleep((t)*1000)
#else
#include <unistd.h>
#endif

// sabitler
extern const uint64_t KAYA_HAFIZA_PARCA_TEMEL_KAPASITE;
extern const uint64_t KAYA_DEGISKEN_ARG_SAYISI_MAKSIMUM;
extern const uint64_t KAYA_NULL_TERMINATOR_BOYUT;
extern char const* const KAYA_DS_YOL_AYRAC;

// tipler
typedef enum
{
    KAYA_BILDIRI_BILGI,
    KAYA_BILDIRI_UYARI,
    KAYA_BILDIRI_HATA,
} KayaBildiriSeviye;

typedef struct
{
    void* isr;
    bool hazir;
    uint64_t boy;
} KayaHafizaParca;

typedef struct
{
    KayaHafizaParca hafiza;
    uint64_t boy;
    uint64_t uye_boy;
} KayaDiziBilgi;

typedef struct
{
    char* bas;
    uint32_t boy;
} KayaStringOrnek;

typedef struct
{
    KayaHafizaParca hafiza;
    uint32_t boy;
    uint32_t uye_boy;
    uint32_t baslangic_indeks;
} KayaFifoBilgi;

#define KAYA_TIPI_YERLESTIR(tip) tip

#define KAYA_DIZI_TANIMLA(tip_ismi, tip_elem_tip) \
typedef struct \
{\
    union\
    {\
        KAYA_TIPI_YERLESTIR(tip_elem_tip)* bas;\
        struct {\
            KayaHafizaParca hafiza_parca;\
            uint32_t boy;\
        };\
        KayaDiziBilgi dizi_bilgi;\
    };\
} KAYA_TIPI_YERLESTIR(tip_ismi)

KAYA_DIZI_TANIMLA(KayaString, char);
KAYA_DIZI_TANIMLA(KayaCstrDizi, char*);
KAYA_DIZI_TANIMLA(KayaStringDizi, KayaString);
KAYA_DIZI_TANIMLA(KayaStringOrnekDizi, KayaStringOrnek);

#define KAYA_FIFO_TANIMLA(tip_ismi, tip_elem_tip) \
typedef struct \
{\
    union\
    {\
        KAYA_TIPI_YERLESTIR(tip_elem_tip)* bas;\
        struct {\
            KayaHafizaParca hafiza;\
            uint32_t boy;\
        };\
        KayaFifoBilgi fifo_bilgi;\
    };\
} KAYA_TIPI_YERLESTIR(tip_ismi)

// https://stackoverflow.com/a/36015150
#ifdef _MSC_VER // Microsoft compilers
#define GET_ARG_COUNT(...)  INTERNAL_EXPAND_ARGS_PRIVATE(INTERNAL_ARGS_AUGMENTER(__VA_ARGS__))
#define INTERNAL_ARGS_AUGMENTER(...) unused, __VA_ARGS__
#define INTERNAL_EXPAND(x) x
#define INTERNAL_EXPAND_ARGS_PRIVATE(...) INTERNAL_EXPAND(INTERNAL_GET_ARG_COUNT_PRIVATE(__VA_ARGS__, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0))
#define INTERNAL_GET_ARG_COUNT_PRIVATE(_1_, _2_, _3_, _4_, _5_, _6_, _7_, _8_, _9_, _10_, _11_, _12_, _13_, _14_, _15_, _16_, _17_, _18_, _19_, _20_, _21_, _22_, _23_, _24_, _25_, _26_, _27_, _28_, _29_, _30_, _31_, _32_, _33_, _34_, _35_, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, _64, _65, _66, _67, _68, _69, _70, count, ...) count
#else // Non-Microsoft compilers
#define GET_ARG_COUNT(...) INTERNAL_GET_ARG_COUNT_PRIVATE(0, ## __VA_ARGS__, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
#define INTERNAL_GET_ARG_COUNT_PRIVATE(_0, _1_, _2_, _3_, _4_, _5_, _6_, _7_, _8_, _9_, _10_, _11_, _12_, _13_, _14_, _15_, _16_, _17_, _18_, _19_, _20_, _21_, _22_, _23_, _24_, _25_, _26_, _27_, _28_, _29_, _30_, _31_, _32_, _33_, _34_, _35_, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, _64, _65, _66, _67, _68, _69, _70, count, ...) count
#endif

typedef enum
{
    KAYA_SONUC_BASARILI,
    KAYA_SONUC_TANIMSIZ,
    KAYA_SONUC_ESNEK_DIZI_YETERSIZ_ALAN,
    KAYA_SONUC_HAFIZA_PARCA_YENIDEN_BOYUTLANDIRMA_DEGISKENI_TASTI,
    KAYA_SONUC_HAFIZA_PARCA_YONETICI_AYIR_BASARISIZ_OLDU,
    KAYA_SONUC_DOSYA_BULUNAMADI,
    KAYA_SONUC_GECERSIZ_PARAMATRE_1,
    KAYA_SONUC_GECERSIZ_PARAMATRE_2,
    KAYA_SONUC_GECERSIZ_PARAMATRE_3,
    KAYA_SONUC_GECERSIZ_PARAMATRE_4,
    KAYA_SONUC_GECERSIZ_PARAMATRE_5,
    KAYA_SONUC_GECERSIZ_PARAMATRE_6,
} KayaSonuc;

static inline char* kaya_hata_kodundan_hata_mesaji(int kod)
{
    switch (kod)
    {
    case KAYA_SONUC_BASARILI:
        return "KAYA_SONUC_BASARILI";
    case KAYA_SONUC_TANIMSIZ:
        return "KAYA_SONUC_TANIMSIZ";
    case KAYA_SONUC_ESNEK_DIZI_YETERSIZ_ALAN:
        return "KAYA_SONUC_ESNEK_DIZI_YETERSIZ_ALAN";
    case KAYA_SONUC_HAFIZA_PARCA_YENIDEN_BOYUTLANDIRMA_DEGISKENI_TASTI:
        return "KAYA_SONUC_HAFIZA_PARCA_YENIDEN_BOYUTLANDIRMA_DEGISKENI_TASTI";
    case KAYA_SONUC_HAFIZA_PARCA_YONETICI_AYIR_BASARISIZ_OLDU:
        return "KAYA_SONUC_HAFIZA_PARCA_YONETICI_AYIR_BASARISIZ_OLDU";
    case KAYA_SONUC_DOSYA_BULUNAMADI:
        return "KAYA_SONUC_DOSYA_BULUNAMADI";
    case KAYA_SONUC_GECERSIZ_PARAMATRE_1:
        return "KAYA_SONUC_GECERSIZ_PARAMATRE_1";
    case KAYA_SONUC_GECERSIZ_PARAMATRE_2:
        return "KAYA_SONUC_GECERSIZ_PARAMATRE_2";
    case KAYA_SONUC_GECERSIZ_PARAMATRE_3:
        return "KAYA_SONUC_GECERSIZ_PARAMATRE_3";
    case KAYA_SONUC_GECERSIZ_PARAMATRE_4:
        return "KAYA_SONUC_GECERSIZ_PARAMATRE_4";
    case KAYA_SONUC_GECERSIZ_PARAMATRE_5:
        return "KAYA_SONUC_GECERSIZ_PARAMATRE_5";
    case KAYA_SONUC_GECERSIZ_PARAMATRE_6:
        return "KAYA_SONUC_GECERSIZ_PARAMATRE_6";
    default:
        return "Kaya bilinmeyen hata";
    }   
}

typedef void(*KayaHafizaParcaTemizleFonk)(KayaHafizaParca *const);
typedef KayaHafizaParca(*KayaHafizaParcaAyirFonk)(uint64_t const);

typedef struct
{
    KayaHafizaParcaTemizleFonk temizle;
    KayaHafizaParcaAyirFonk ayir;
} KayaHafizaYonetici;

typedef enum
{
    KAYA_DOSYA_AKIS_KAPALI,
    KAYA_DOSYA_AKIS_DEVAM_EDIYOR,
    KAYA_DOSYA_AKIS_TAMAMLANDI,
} KayaDosyaAkisDurum;

typedef struct
{
    FILE* dosya;
    uint64_t dosya_boyut;
    uint64_t okunan;
    KayaDosyaAkisDurum durum;
} KayaDosyaAkis;

typedef enum
{
    KAYA_DOSYA_AKIS_BASLATMA_SONUC_BASARILI,
    KAYA_DOSYA_AKIS_BASLATMA_SONUC_DOSYA_BULUNAMADI,
    KAYA_DOSYA_AKIS_BASLATMA_SONUC_DOSYA_ACILAMADI,
} KayaDosyaAkisBaslatmaSonucDurum;

typedef struct
{
    KayaDosyaAkisBaslatmaSonucDurum durum;
    KayaDosyaAkis akis;
} KayaDosyaAkisBaslatmaSonuc;

typedef enum
{
    KAYA_DOSYA_ISTATISTIK_SONUC_BASARILI,
    KAYA_DOSYA_ISTATISTIK_SONUC_DOSYA_BULUNAMADI,
    KAYA_DOSYA_ISTATISTIK_SONUC_GECERSIZ_PARAMATRE_2,
    KAYA_DOSYA_ISTATISTIK_SONUC_TANIMSIZ,
} KayaDosyaIstatistikSonucDurum;

typedef struct
{
    KayaDosyaIstatistikSonucDurum durum;
    struct stat sonuc;
} KayaDosyaIstatistikSonuc;

#ifndef KAYA_DAGITIM_VERSIYONU
/*
calisma zamani assertion gibi ancak dagitimda kaldiriliyor
#define KAYA_GELISTIRME_KONTROL_ET_BILDIR(durum, seviye, mesaj_format, ...) do{\
    if((durum))\
    {\
        kaya_bildir_gelismis((seviye), __FILE__, __LINE__, mesaj_format, ##__VA_ARGS__);\
        if((seviye) == KAYA_BILDIRI_HATA)\
            exit(1);\
    }\
}while(0)
#else
#define KAYA_GELISTIRME_KONTROL_ET_BILDIR(durum, seviye, mesaj_format, ...)
#endif
warning: ISO C99 requires at least one argument for the "..." in a variadic macro
uyarsini durdurmak icin asagidaki sekle getirildi. Kullanimi ayni
*/
#define KAYA_GELISTIRME_KONTROL_ET_BILDIR(durum, seviye, ...) do{\
    if((durum))\
    {\
        kaya_bildir_gelismis((seviye), __FILE__, __LINE__, ##__VA_ARGS__);\
        if((seviye) == KAYA_BILDIRI_HATA)\
            exit(1);\
    }\
}while(0)
#else
#define KAYA_GELISTIRME_KONTROL_ET_BILDIR(durum, seviye, mesaj_format, ...)
#endif

#define kaya_dizi_ekle(dizi_ptr, deg) \
    (dizi_ptr)->bas[(dizi_ptr)->boy++] = deg

#define kaya_dizi_sil(dizi_ptr, indeks, elem_ptr) \
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(\
        (indeks >= (dizi_ptr)->boy) || (indeks < 0),\
        KAYA_BILDIRI_HATA,\
        "indeks dizi sinirlari icinde olmali!"\
    );\
    if((elem_ptr) != NULL)\
    {\
        *(elem_ptr) = (dizi_ptr)->bas[indeks];\
    }\
    for(uint32_t i = indeks; i < (dizi_ptr)->boy; ++i) \
    {\
        (dizi_ptr)->bas[i] = (dizi_ptr)->bas[i + 1];\
    }\
    --(dizi_ptr)->boy

#define kaya_dizi_coklu_sil(dizi_ptr, bindeks, sindeks) \
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(\
        (bindeks) > (sindeks),\
        KAYA_BILDIRI_HATA,\
        "baslangic indeks bitisten buyuk olamaz!"\
    );\
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(\
        (dizi_ptr) == NULL,\
        KAYA_BILDIRI_HATA,\
        "dizi_ptr NULL olamaz!"\
    );\
    for(int i = bindeks; i < (dizi_ptr)->boy - ((sindeks) - (bindeks) - 1); ++i) \
    {\
        (dizi_ptr)->bas[i] = (dizi_ptr)->bas[i + ((sindeks) - (bindeks))];\
    }\
    (dizi_ptr)->boy -= ((sindeks) - (bindeks))

#if (__STDC_VERSION__ >= 202311L || __GNUC__)
#define kaya_dizi_coklu_ekle(dizi_ptr, ...) do{\
    typeof((dizi_ptr)->bas[0]) __dizi_adi[] = { __VA_ARGS__ };\
    for(size_t i = 0; i < sizeof(__dizi_adi)/sizeof(__dizi_adi[0]); ++i)\
    {\
        kaya_dizi_ekle((dizi_ptr), __dizi_adi[i]);\
    }\
}while(0)
#endif

#define kaya_dizi_sifirla(dizi_ptr) \
    (dizi_ptr)->boy = 0

#define kaya_dizi_yer_var_mi(dizi, sayi) kaya_dizi_yer_var_mi_fonk(&(dizi).dizi_bilgi, (sayi))

#define kaya_dizi_genislet(dizi, sayi) \
    kaya_esnek_hafiza_gerekli_boyutu_karsila(&dizi.hafiza_parca, \
    (dizi.boy + sayi) * dizi.dizi_bilgi.uye_boy \
)

#define kaya_dizi_hazirla(dizi_ptr) \
    kaya_dizi_hazirla_gelismis(\
        &(dizi_ptr)->dizi_bilgi,\
        sizeof((dizi_ptr)->bas[0]),\
        kaya_yeterli_hafiza_parca_getir(\
            sizeof((dizi_ptr)->bas[0]),\
            KAYA_HAFIZA_PARCA_TEMEL_KAPASITE)\
    )

#define kaya_dizi_hazirla_boyut(dizi_ptr, boyut) \
    kaya_dizi_hazirla_gelismis(\
        &(dizi_ptr)->dizi_bilgi,\
        sizeof((dizi_ptr)->bas[0]),\
        kaya_yeterli_hafiza_parca_getir(sizeof((dizi_ptr)->bas[0]), (boyut))\
    )

#define kaya_dizi_temizle(dizi) \
    kaya_dizi_temizle_gelismis_fonk(kaya_temel_hafiza_yonetici, &dizi.dizi_bilgi)

#define kaya_dizi_temizle_gelismis(hafiza_yonetici, dizi) \
    kaya_dizi_temizle_gelismis_fonk(hafiza_yonetici, &dizi.dizi_bilgi)


#define kaya_fifo_hazirla(fifo_ptr) \
    kaya_fifo_hazirla_gelismis(\
        &(fifo_ptr)->fifo_bilgi,\
        sizeof((fifo_ptr)->bas[0]),\
        kaya_yeterli_hafiza_parca_getir(\
            sizeof((fifo_ptr)->bas[0]),\
            KAYA_HAFIZA_PARCA_TEMEL_KAPASITE)\
    )

#define kaya_fifo_hazirla_boyut(fifo_ptr, boyut) \
    kaya_fifo_hazirla_gelismis(\
        &(fifo_ptr)->fifo_bilgi,\
        sizeof((fifo_ptr)->bas[0]),\
        kaya_yeterli_hafiza_parca_getir(sizeof((fifo_ptr)->bas[0]), (boyut))\
    )

#define kaya_fifo_ekle(fifo_ptr, val) do{                                 \
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(                                    \
        (fifo_ptr)->fifo_bilgi.boy >= (fifo_ptr)->hafiza.boy / (fifo_ptr)->fifo_bilgi.uye_boy, \
        KAYA_BILDIRI_HATA,                                                \
        "%s fifo dolu oldugu halde ekleme yapildi", #fifo_ptr            \
    );                                                                    \
    uint32_t ekleme_konum =                                               \
        (fifo_ptr)->fifo_bilgi.baslangic_indeks + (fifo_ptr)->fifo_bilgi.boy; \
    (fifo_ptr)->bas[ekleme_konum % (fifo_ptr)->hafiza.boy] = val;           \
    (fifo_ptr)->fifo_bilgi.boy++; \
}while(0)

#define kaya_fifo_yeterli_yer_var_mi(fifo, size) \
    fifo.fifo_bilgi.boy + size <= fifo.hafiza.boy / fifo.fifo_bilgi.uye_boy

#define kaya_fifo_yer_var_mi(fifo) \
    kaya_fifo_yeterli_yer_var_mi(fifo, 1)

#define kaya_fifo_temizle(fifo_ptr) do{ \
    (fifo_ptr)->fifo_bilgi.baslangic_indeks = 0; \
    (fifo_ptr)->fifo_bilgi.boy = 0; \
} while(0)

#define kaya_fifo_eleman_var_mi(fifo) \
    fifo.fifo_bilgi.boy > 0

#define kaya_fifo_cikar(fifo_ptr, val_ptr) do{\
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(\
        (fifo_ptr)->fifo_bilgi.boy == 0,\
        KAYA_BILDIRI_HATA, \
        "%s fifosu bos iken eleman alinmaya calisildi", #fifo_ptr \
    );\
    *(val_ptr) = (fifo_ptr)->bas[(fifo_ptr)->fifo_bilgi.baslangic_indeks++]; \
    (fifo_ptr)->fifo_bilgi.boy--; \
} while(0)

#define kaya_fifo_ekle_buf(fifo_ptr, buf, len) do{\
    size_t fifo_kapasite = (fifo_ptr)->hafiza.boy / (fifo_ptr)->fifo_bilgi.uye_boy;\
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(\
        (fifo_ptr)->boy + (len) > fifo_kapasite,\
        KAYA_BILDIRI_HATA,\
        "fifo tasmasi engellendi kap: %d boy: %d eklenen: %d",\
        fifo_kapasite,\
        (fifo_ptr)->boy,\
        (len)\
    );\
    for(size_t i = 0; i < (len); ++i)\
    {\
        uint32_t indeks = \
            ((fifo_ptr)->fifo_bilgi.baslangic_indeks + i) % (fifo_kapasite);\
        (fifo_ptr)->bas[indeks] = buf[i];\
    }\
    (fifo_ptr)->boy += (len);\
} while(0)

#if (__STDC_VERSION__ >= 202311L || __GNUC__)
#define kaya_fifo_coklu_ekle(dizi_ptr, ...) do{                             \
    typeof((dizi_ptr)->bas[0]) __dizi_adi[] = { __VA_ARGS__ };              \
    for(size_t i = 0; i < sizeof(__dizi_adi)/sizeof(__dizi_adi[0]); ++i)    \
    {                                                                       \
        kaya_fifo_ekle((dizi_ptr), __dizi_adi[i]);                          \
    }                                                                       \
}while(0)
#endif

#define KAYA_FIFO_FOR_EACH(fifo, elem_ptr) \
    for( \
        elem_ptr = &fifo.bas[fifo.fifo_bilgi.baslangic_indeks]; \
        fifo.fifo_bilgi.boy > 0 && \
        elem_ptr != &fifo.bas[(fifo.fifo_bilgi.baslangic_indeks + fifo.fifo_bilgi.boy) % fifo.hafiza.boy];\
        elem_ptr = &fifo.bas[(fifo.fifo_bilgi.baslangic_indeks + abs((int)(elem_ptr - fifo.bas)) + 1) % fifo.hafiza.boy] \
    )

// fonksiyon beyanlari
void kaya_bildir_gelismis(
    KayaBildiriSeviye const seviye,
    char* dosya_adi,
    uint32_t satir_sayisi,
    char const* const mesaj_formati,
    ...
);

/*
#define kaya_bildir(seviye, mesaj_formati, ...) \
    kaya_bildir_gelismis((seviye), __FILE__, __LINE__, mesaj_formati, ##__VA_ARGS__)
warning: ISO C99 requires at least one argument for the "..." in a variadic macro
uyarisini durdurmak amaciyla asagidaki sekle getirildi. Kullanimi ayni
*/
#define kaya_bildir(seviye, ...) \
    kaya_bildir_gelismis((seviye), __FILE__, __LINE__, ##__VA_ARGS__)

KayaHafizaParca kaya_temel_hafiza_yonetici_parca_ayir(
    uint64_t const boyut
);

void kaya_fifo_hazirla_gelismis(
    KayaFifoBilgi* const fifo,
    uint64_t const uye_boy,
    KayaHafizaParca const p
);

void kaya_temel_hafiza_yonetici_temizle(
    KayaHafizaParca* const p
);

KayaSonuc kaya_esnek_hafiza_gerekli_boyutu_karsila_gelismis(
    KayaHafizaYonetici const hafiza_yonetici,
    KayaHafizaParca* const hafiza_parca,
    uint64_t const gerekli_boyut
);

static const KayaHafizaYonetici kaya_temel_hafiza_yonetici = {
    .temizle = kaya_temel_hafiza_yonetici_temizle,
    .ayir = kaya_temel_hafiza_yonetici_parca_ayir,
};

KayaSonuc kaya_esnek_hafiza_gerekli_boyutu_karsila(
    KayaHafizaParca *const hafiza_parca,
    uint64_t const gerekli_boyut
);

bool kaya_dizi_yer_var_mi_fonk(
    KayaDiziBilgi const *const dizi,
    uint64_t const sayi
);

void kaya_dizi_temizle_gelismis_fonk(
    KayaHafizaYonetici const hafiza_yonetici,
    KayaDiziBilgi *const dizi
);

void kaya_dizi_hazirla_gelismis(
    KayaDiziBilgi* const dizi,
    uint64_t const uye_boy,
    KayaHafizaParca const p
);

KayaHafizaParca kaya_yeterli_hafiza_parca_getir_gelismis(
    KayaHafizaYonetici const hafiza_yonetici,
    uint64_t const uye_boy,
    uint64_t const uye_adet
);

KayaHafizaParca kaya_yeterli_hafiza_parca_getir(
    uint64_t const uye_boy,
    uint64_t const uye_adet
);

void kaya_string_hazirla(KayaString *const str);

void kaya_string_hazirla_gelismis(
    KayaHafizaYonetici const hafiza_yonetici,
    KayaString *const str,
    uint32_t const boyut
);

void kaya_string_hazirla_boyut(
    KayaString *const str,
    uint32_t const boyut
);

void kaya_string_hazirla_buf(
    KayaString *const str,
    void* buf,
    uint64_t buf_boyut
);

bool kaya_string_yeterli_mi(KayaString const str, uint32_t const boyut);

void kaya_string_ekle_cstr(KayaString *const str, char const *const ek);

void kaya_string_ekle_kso(KayaString *const str, KayaStringOrnek const ek);

void kaya_string_ekle_ks(KayaString *const str, KayaString const ek);

#define kaya_string_ekle(str, X) _Generic((X), \
    char*: kaya_string_ekle_cstr,\
    char const*: kaya_string_ekle_cstr,\
    KayaStringOrnek: kaya_string_ekle_kso,\
    KayaString: kaya_string_ekle_ks\
)(str, X)

void kaya_string_coklu_ekle_cstr_n(KayaString *const str, char const *const str2, ...);

void kaya_string_coklu_ekle_ks_n(KayaString* str, KayaString const str2, ...);

void kaya_string_coklu_ekle_kso_n(KayaString* str, KayaStringOrnek const str2, ...);

#define kaya_string_coklu_ekle_ks(str, str2, ...) \
    kaya_string_ekle_ks_n(str, str2, __VA_ARGS__, kaya_null_kaya_string)

#define kaya_string_coklu_ekle_cstr(str, str2, ...) \
    kaya_string_coklu_ekle_cstr_n(str, str2, __VA_ARGS__, NULL)

#define kaya_string_coklu_ekle_kso(str, str2, ...) \
    kaya_string_coklu_ekle_kso_n(str, str2, __VA_ARGS__, kaya_null_kaya_string_ornek)

#define kaya_string_coklu_ekle(str, X, ...) _Generic((X), \
    char*: kaya_string_coklu_ekle_cstr_n,\
    char const*: kaya_string_coklu_ekle_cstr_n,\
    KayaStringOrnek: kaya_string_coklu_ekle_kso_n,\
    KayaString: kaya_string_coklu_ekle_ks_n \
)(str, X, GET_ARG_COUNT(__VA_ARGS__), __VA_ARGS__)

KayaSonuc kaya_string_genislet(
    KayaString* const str,
    uint32_t const boyut
);

void kaya_string_temizle(KayaString* const str);

void kaya_string_temizle_gelismis(
    KayaHafizaYonetici const hafiza_yonetici, 
    KayaString* const str
);

KayaStringOrnek kaya_ds_dosya_adini_getir_ks(KayaString const str);

KayaStringOrnek kaya_ds_dosya_adini_getir_kso(KayaStringOrnek const str);

KayaStringOrnek kaya_ds_dosya_adini_getir_cstr(char *const str);

#define kaya_ds_dosya_adini_getir(X) _Generic((X), \
    KayaString: kaya_ds_dosya_adini_getir_ks, \
    KayaStringOrnek: kaya_ds_dosya_adini_getir_kso, \
    char const*: kaya_ds_dosya_adini_getir_cstr, \
    char *: kaya_ds_dosya_adini_getir_cstr \
)(X)

KayaStringOrnek kaya_ds_dosya_uzantisini_getir_ks(KayaString const str);

KayaStringOrnek kaya_ds_dosya_uzantisini_getir_kso(KayaStringOrnek const str);

KayaStringOrnek kaya_ds_dosya_uzantisini_getir_cstr(char *const str);

#define kaya_ds_dosya_uzantisini_getir(X) _Generic((X), \
    KayaString: kaya_ds_dosya_uzantisini_getir_ks, \
    KayaStringOrnek: kaya_ds_dosya_uzantisini_getir_kso, \
    char const*: kaya_ds_dosya_uzantisini_getir_cstr, \
    char *: kaya_ds_dosya_uzantisini_getir_cstr \
) (X)

KayaDosyaIstatistikSonuc kaya_ds_dosya_istatistik_cstr(char const* const str);

KayaDosyaIstatistikSonuc kaya_ds_dosya_istatistik_ks(KayaString const str);

KayaDosyaIstatistikSonuc kaya_ds_dosya_istatistik_kso(KayaStringOrnek const str);

#define kaya_ds_dosya_istatistik(X) _Generic((X), \
    KayaStringOrnek: kaya_ds_dosya_istatistik_kso,\
    KayaString: kaya_ds_dosya_istatistik_ks, \
    char const*: kaya_ds_dosya_istatistik_cstr, \
    char *const: kaya_ds_dosya_istatistik_cstr, \
    char*: kaya_ds_dosya_istatistik_cstr \
)(X)

bool kaya_ds_dosya_var_mi_ks(KayaString const str);

bool kaya_ds_dosya_var_mi_kso(KayaStringOrnek const str);

bool kaya_ds_dosya_var_mi_cstr(char const *const str);

#define kaya_ds_dosya_var_mi(X) _Generic((X), \
    KayaString: kaya_ds_dosya_var_mi_ks, \
    KayaStringOrnek: kaya_ds_dosya_var_mi_kso, \
    char const*: kaya_ds_dosya_var_mi_cstr, \
    char *: kaya_ds_dosya_var_mi_cstr, \
    char *const: kaya_ds_dosya_var_mi_cstr \
)(X)

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr(
    char *const str,
    char *const str2
);

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_cstr_kso(
    char *const str,
    KayaStringOrnek const str2
);

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_cstr_ks(
    char *const str,
    KayaString const str2
);

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_kso_cstr(
    KayaStringOrnek const str,
    char *const str2
);

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_kso_kso(
    KayaStringOrnek const str,
    KayaStringOrnek const str2
);

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_kso_ks(
    KayaStringOrnek const str,
    KayaString const str2
);

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_ks_cstr(
    KayaString const str,
    char *const str2
);

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_ks_kso(
    KayaString const str,
    KayaStringOrnek const str2
);

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_ks_ks(
    KayaString const str,
    KayaString const str2
);

#define kaya_ds_dosya_duzenlenme_zamani_farki(X, Y) _Generic((X), \
    char *: \
        _Generic((Y), \
            char*: kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr, \
            char const*: kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr, \
            KayaStringOrnek: kaya_ds_dosya_duzenlenme_zamani_farki_cstr_kso,\
            KayaString: kaya_ds_dosya_duzenlenme_zamani_farki_cstr_ks\
        ),\
    char const*: \
        _Generic((Y), \
            char*: kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr, \
            char const*: kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr, \
            KayaStringOrnek: kaya_ds_dosya_duzenlenme_zamani_farki_cstr_kso,\
            KayaString: kaya_ds_dosya_duzenlenme_zamani_farki_cstr_ks \
        ), \
    KayaStringOrnek: \
        _Generic((Y), \
            char*: kaya_ds_dosya_duzenlenme_zamani_farki_kso_cstr, \
            char const*: kaya_ds_dosya_duzenlenme_zamani_farki_kso_cstr, \
            KayaStringOrnek: kaya_ds_dosya_duzenlenme_zamani_farki_kso_kso,\
            KayaString: kaya_ds_dosya_duzenlenme_zamani_farki_kso_ks \
        ), \
    KayaString: \
        _Generic((Y), \
            char*: kaya_ds_dosya_duzenlenme_zamani_farki_ks_cstr, \
            char const*: kaya_ds_dosya_duzenlenme_zamani_farki_ks_cstr, \
            KayaStringOrnek: kaya_ds_dosya_duzenlenme_zamani_farki_ks_kso,\
            KayaString: kaya_ds_dosya_duzenlenme_zamani_farki_ks_ks \
        ) \
)(X, Y)

#define kaya_ds_dosya_boyutunu_getir(X) _Generic((X), \
    KayaString: kaya_ds_dosya_boyutunu_getir_ks, \
    KayaStringOrnek: kaya_ds_dosya_boyutunu_getir_kso, \
    char const*: kaya_ds_dosya_boyutunu_getir_cstr, \
    char *: kaya_ds_dosya_boyutunu_getir_cstr \
)(X)

uint32_t kaya_ds_dosya_boyutunu_getir_cstr(char const* dosya_adi);

uint32_t kaya_ds_dosya_boyutunu_getir_kso(KayaStringOrnek const dosya_adi);

uint32_t kaya_ds_dosya_boyutunu_getir_ks(KayaString const dosya_adi);

KayaDosyaAkisBaslatmaSonuc kaya_ds_dosya_akisini_baslat_cstr(char const* const dosya_adi);

KayaDosyaAkisBaslatmaSonuc kaya_ds_dosya_akisini_baslat_kso(KayaStringOrnek const dosya_adi);

KayaDosyaAkisBaslatmaSonuc kaya_ds_dosya_akisini_baslat_ks(KayaString const dosya_adi);

#define kaya_ds_dosya_akisini_baslat(X) _Generic((X),\
    char*: kaya_ds_dosya_akisini_baslat_cstr,\
    char const*: kaya_ds_dosya_akisini_baslat_cstr,\
    KayaStringOrnek: kaya_ds_dosya_akisini_baslat_kso,\
    KayaString: kaya_ds_dosya_akisini_baslat_ks\
)(X)

void kaya_ds_dosya_akisini_bitir(KayaDosyaAkis const* akis);

void kaya_ds_dosya_akisindan_oku_buf(KayaDosyaAkis* akis, void* buf, uint64_t maks_boyut);

void kaya_bsis_gerekliyse_tekrar_derle_fonk(char* cagrilan_dosya_adi, int argc, char*argv[]);

#define kaya_bsis_gerekliyse_tekrar_derle(argc, argv) \
    kaya_bsis_gerekliyse_tekrar_derle_fonk(__FILE__, argc, argv)

typedef struct
{
    KayaStringDizi cflags;
    KayaStringDizi lflags;
    KayaStringDizi srcs;
    KayaStringDizi komutlar;
    KayaString hedef_adi;
    KayaString cikti_klasoru;
    KayaString derleyici;
} KayaBsisHedef;

#define kaya_bsis_string_ayarla(hedef_ptr, X) _Generic((X), \
    char*: kaya_bsis_string_ayarla_cstr,\
    char const*: kaya_bsis_string_ayarla_cstr,\
    KayaStringOrnek: kaya_bsis_string_ayarla_kso,\
    KayaString: kaya_bsis_string_ayarla_ks\
)(hedef_ptr, X)

void kaya_bsis_string_ayarla_cstr(KayaString* hedef, char const* const str);
void kaya_bsis_string_ayarla_kso(KayaString* hedef, KayaStringOrnek const str);
void kaya_bsis_string_ayarla_ks(KayaString* hedef, KayaString const str);

#define kaya_bsis_hedef_ayarla(bsis_hedef_ptr, str) \
    kaya_bsis_string_ayarla(&(bsis_hedef_ptr)->hedef_adi, str)

#define kaya_bsis_cikti_klasoru_ayarla(bsis_hedef_ptr, str) \
    kaya_bsis_string_ayarla(&(bsis_hedef_ptr)->cikti_klasoru, str)

#define kaya_bsis_derleyici_ayarla(bsis_hedef_ptr, str) \
    kaya_bsis_string_ayarla(&(bsis_hedef_ptr)->derleyici, str)

#define kaya_bsis_string_dizi_ayarla(bsis_hedef_ptr, DIZI) _Generic((DIZI), \
    char**: kaya_bsis_string_dizi_ayarla_cstr, \
    char const**: kaya_bsis_string_dizi_ayarla_cstr, \
    KayaStringOrnekDizi: kaya_bsis_string_dizi_ayarla_kso, \
    KayaStringDizi: kaya_bsis_string_dizi_ayarla_ks \
)(bsis_hedef_ptr, DIZI, (sizeof(DIZI) / sizeof(DIZI[0])))

#define kaya_bsis_cflags_ayarla(bsis_hedef_ptr, str_dizi) \
    kaya_bsis_string_dizi_ayarla(&(bsis_hedef_ptr)->cflags, str_dizi)

#define kaya_bsis_lflags_ayarla(bsis_hedef_ptr, str_dizi) \
    kaya_bsis_string_dizi_ayarla(&(bsis_hedef_ptr)->lflags, str_dizi)

#define kaya_bsis_srcs_ayarla(bsis_hedef_ptr, str_dizi) \
    kaya_bsis_string_dizi_ayarla(&(bsis_hedef_ptr)->srcs, str_dizi)

void kaya_bsis_string_dizi_ayarla_cstr(KayaStringDizi* hedef, char** str_dizi, uint32_t adet);

void kaya_bsis_string_dizi_ayarla_kso(
    KayaStringDizi* hedef,
    KayaStringOrnekDizi str_dizi,
    uint32_t adet
);

void kaya_bsis_string_dizi_ayarla_ks(
    KayaStringDizi* hedef,
    KayaStringDizi str_dizi,
    uint32_t adet
);

void kaya_bsis_komutlari_uret(
    KayaBsisHedef* hedef
);

#ifdef KAYA_TANIMLAR
////////////////////////////////
//                            //
//                            //
//          TANIMLAR          //
//                            //
//                            //
////////////////////////////////

// sabitler
const uint64_t KAYA_HAFIZA_PARCA_TEMEL_KAPASITE = 128;
const uint64_t KAYA_DEGISKEN_ARG_SAYISI_MAKSIMUM = 32;
const uint64_t KAYA_NULL_TERMINATOR_BOYUT = 1;


#ifdef _WIN32
char const* const KAYA_DS_YOL_AYRAC = "/";
#else
char const* const KAYA_DS_YOL_AYRAC = "/";
#endif 

void kaya_bildir_gelismis(
    KayaBildiriSeviye const seviye,
    char* dosya_adi,
    uint32_t satir_sayisi,
    char const* const mesaj_formati,
    ...
)
{
    va_list liste;
    va_start(liste, mesaj_formati);
    switch (seviye)
    {
    case KAYA_BILDIRI_BILGI:
        printf("[ BILGI ] ");
        break;
    case KAYA_BILDIRI_UYARI:
        printf("[ UYARI ] ");
        break;
    case KAYA_BILDIRI_HATA:
        printf("[ HATA ] ");
        break;
    default:
        break;
    }

    vprintf(mesaj_formati, liste);
    printf(" (%s:%u)\n", dosya_adi, satir_sayisi);
    va_end(liste);
}

KayaHafizaParca kaya_temel_hafiza_yonetici_parca_ayir(
    uint64_t const boyut
)
{
    KayaHafizaParca sonuc = {0};
    void* hafiza = malloc(boyut);
    if(hafiza != NULL)
    {
        sonuc.hazir = true;
        sonuc.boy = boyut;
        sonuc.isr = hafiza;
    }

    return sonuc;
}

void kaya_fifo_hazirla_gelismis(
    KayaFifoBilgi* const fifo,
    uint64_t const uye_boy,
    KayaHafizaParca const p
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        (
            fifo->boy != 0 ||
            fifo->hafiza.isr != 0 ||
            fifo->hafiza.boy != 0 ||
            fifo->hafiza.hazir != 0 ||
            fifo->uye_boy != 0 ||
            fifo->baslangic_indeks != 0
        ),
        KAYA_BILDIRI_HATA,
        "ilklendirilmemis fifo yapisi!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        p.hazir == false,
        KAYA_BILDIRI_HATA,
        "verilen hafiza gecersiz!"
    );

    fifo->hafiza = p;
    fifo->hafiza.hazir = true;
    fifo->uye_boy = uye_boy;
    fifo->baslangic_indeks = 0;
}

void kaya_temel_hafiza_yonetici_temizle(KayaHafizaParca *const p)
{
    if(p->hazir)
    {
        free(p->isr);
        p->hazir = false;
        p->boy = 0;
    }
}

KayaSonuc kaya_esnek_hafiza_gerekli_boyutu_karsila_gelismis(
    KayaHafizaYonetici const hafiza_yonetici,
    KayaHafizaParca* const hafiza_parca,
    uint64_t const gerekli_boyut
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        hafiza_parca == NULL,
        KAYA_BILDIRI_HATA,
        "hafiza_parca parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        hafiza_yonetici.ayir == NULL,
        KAYA_BILDIRI_HATA,
        "hafiza_yonetici.ayir parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        hafiza_yonetici.temizle == NULL,
        KAYA_BILDIRI_HATA,
        "hafiza_yonetici.temizle parametresi NULL olamaz!"
    );

    KayaSonuc sonuc = KAYA_SONUC_BASARILI;

    if(hafiza_parca->boy < gerekli_boyut)
    {
        // uint64_t yeni_kapasite = KAYA_HAFIZA_PARCA_TEMEL_KAPASITE;
        // while (yeni_kapasite < gerekli_boyut && sonuc != KAYA_SONUC_HAFIZA_PARCA_YENIDEN_BOYUTLANDIRMA_DEGISKENI_TASTI)
        // {
        //     printf("ykps: %lld, gb: %lld\n", yeni_kapasite, gerekli_boyut);
        //     if(LONG_LONG_MAX / 2 >= yeni_kapasite)
        //     {
        //         yeni_kapasite *= 2;
        //     }
        //     else
        //     {
        //         sonuc = KAYA_SONUC_HAFIZA_PARCA_YENIDEN_BOYUTLANDIRMA_DEGISKENI_TASTI;
        //     }          
        // }

        uint64_t yeni_kapasite = gerekli_boyut;

        if(sonuc != KAYA_SONUC_HAFIZA_PARCA_YENIDEN_BOYUTLANDIRMA_DEGISKENI_TASTI)
        {
            KayaHafizaParca yeni = {0};
            yeni = hafiza_yonetici.ayir(yeni_kapasite);

            if(yeni.hazir == false)
            {
                sonuc = KAYA_SONUC_HAFIZA_PARCA_YONETICI_AYIR_BASARISIZ_OLDU;
            }
            else
            {
                if(hafiza_parca->hazir)
                {
                    memcpy(yeni.isr, hafiza_parca->isr, hafiza_parca->boy);
                    hafiza_yonetici.temizle(hafiza_parca);
                }

                *hafiza_parca = yeni;
                sonuc = KAYA_SONUC_BASARILI;
            }
        }
    }
    else
    {
        sonuc = KAYA_SONUC_BASARILI;
    }

    return sonuc;
}

KayaSonuc kaya_esnek_hafiza_gerekli_boyutu_karsila(
    KayaHafizaParca *const hafiza_parca,
    uint64_t const gerekli_boyut
)
{
    return kaya_esnek_hafiza_gerekli_boyutu_karsila_gelismis(
        kaya_temel_hafiza_yonetici,
        hafiza_parca,
        gerekli_boyut
    );
}

bool kaya_dizi_yer_var_mi_fonk(
    KayaDiziBilgi const *const dizi,
    uint64_t const sayi
)
{
    return ((dizi->boy + sayi) * dizi->uye_boy) <= dizi->hafiza.boy;    
}

void kaya_dizi_temizle_gelismis_fonk(
    KayaHafizaYonetici const hafiza_yonetici,
    KayaDiziBilgi *const dizi)
{
    hafiza_yonetici.temizle(&dizi->hafiza);
}

void kaya_dizi_hazirla_gelismis(
    KayaDiziBilgi* const dizi,
    uint64_t const uye_boy,
    KayaHafizaParca const p
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        (
            dizi->boy != 0 || 
            dizi->hafiza.isr != 0 || 
            dizi->hafiza.boy != 0 || 
            dizi->hafiza.hazir != 0 || 
            dizi->uye_boy != 0
        ),
        KAYA_BILDIRI_HATA,
        "ilklendirilmemis dizi yapisi!"
    );

    dizi->hafiza = p;
    dizi->hafiza.hazir = true;
    dizi->uye_boy = uye_boy;
}

KayaHafizaParca kaya_yeterli_hafiza_parca_getir_gelismis(
    KayaHafizaYonetici const hafiza_yonetici,
    uint64_t const uye_boy,
    uint64_t const uye_adet
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        hafiza_yonetici.ayir == NULL,
        KAYA_BILDIRI_HATA,
        "hafiza_yonetici.ayir parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        hafiza_yonetici.temizle == NULL,
        KAYA_BILDIRI_HATA,
        "hafiza_yonetici.temizle parametresi NULL olamaz!"
    );

    KayaHafizaParca sonuc = {0};

    KayaSonuc s = kaya_esnek_hafiza_gerekli_boyutu_karsila_gelismis(
        hafiza_yonetici,
        &sonuc,
        uye_boy * uye_adet
    );

    if(s == KAYA_SONUC_BASARILI)
    {
        sonuc.hazir = true;
    }

    return sonuc;
}

KayaHafizaParca kaya_yeterli_hafiza_parca_getir(
    uint64_t const uye_boy,
    uint64_t const uye_adet
)
{
    return kaya_yeterli_hafiza_parca_getir_gelismis(
        kaya_temel_hafiza_yonetici,
        uye_boy,
        uye_adet
    );
}

void kaya_string_hazirla(KayaString *const str)
{
    kaya_string_hazirla_boyut(str, KAYA_HAFIZA_PARCA_TEMEL_KAPASITE);
}

void kaya_string_hazirla_boyut(
    KayaString *const str,
    uint32_t const boyut
)
{
    kaya_string_hazirla_gelismis(
        kaya_temel_hafiza_yonetici, 
        str,
        boyut
    );
}

void kaya_string_hazirla_gelismis(
    KayaHafizaYonetici const hafiza_yonetici,
    KayaString *const str,
    uint32_t const boyut
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        (
            str->boy != 0 || 
            str->hafiza_parca.isr != 0 || 
            str->hafiza_parca.boy != 0 || 
            str->hafiza_parca.hazir != 0 || 
            str->dizi_bilgi.uye_boy != 0
        ),
        KAYA_BILDIRI_HATA,
        "ilklendirilmemis KayaString yapisi!"
    );

    str->hafiza_parca = kaya_yeterli_hafiza_parca_getir_gelismis(
        hafiza_yonetici,
        sizeof(char),
        boyut
    );

    if(str->hafiza_parca.hazir == false)
    {
        kaya_bildir(
            KAYA_BILDIRI_UYARI,
            "kaya_string_hazirla hafiza ayrilamadi"
        );
    }

    str->dizi_bilgi.uye_boy = sizeof(char);
}

void kaya_string_hazirla_buf(
    KayaString *const str,
    void* buf,
    uint64_t buf_boyut
)
{
    str->hafiza_parca = (KayaHafizaParca){
        .isr = buf,
        .boy = buf_boyut,
        .hazir = true
    };
    str->boy = 0;
    str->dizi_bilgi.uye_boy = sizeof(char);
}

bool kaya_string_yeterli_mi(KayaString const str, uint32_t const boyut)
{
    return (str.dizi_bilgi.boy + KAYA_NULL_TERMINATOR_BOYUT) * sizeof(char) + boyut <= str.hafiza_parca.boy;
}

void kaya_string_ekle_cstr(KayaString *const str, char const *const ek)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str->bas == NULL,
        KAYA_BILDIRI_HATA,
        "str->bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        ek == NULL,
        KAYA_BILDIRI_HATA,
        "ek parametresi NULL olamaz!"
    );

    uint32_t boyut = strlen(ek);

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        false == kaya_string_yeterli_mi(*str, boyut),
        KAYA_BILDIRI_HATA,
        "str alani yeterli degil genisletilmeli!"
    );

    memcpy(str->bas + str->boy * sizeof(char), ek, boyut);
    str->boy += boyut;
    str->bas[str->boy * sizeof(char)] = 0;
}

void kaya_string_ekle_kso(KayaString *const str, KayaStringOrnek const ek)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str->bas == NULL,
        KAYA_BILDIRI_HATA,
        "str->bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        ek.bas == NULL,
        KAYA_BILDIRI_HATA,
        "ek.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        ek.boy == 0,
        KAYA_BILDIRI_UYARI,
        "ek.boy degeri 0"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        false == kaya_string_yeterli_mi(*str, ek.boy),
        KAYA_BILDIRI_HATA,
        "str alani yeterli degil genisletilmeli!"
    );

    memcpy(str->bas + str->boy * sizeof(char), ek.bas, ek.boy);
    str->boy += ek.boy;
    str->bas[str->boy * sizeof(char)] = 0;
}

void kaya_string_ekle_ks(KayaString *const str, KayaString const ek)
{
    kaya_string_ekle_kso(str, (KayaStringOrnek){.bas = ek.bas, .boy = ek.boy});
}

void kaya_string_coklu_ekle_kso_n(KayaString* str, KayaStringOrnek const str2, ...)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str->bas == NULL,
        KAYA_BILDIRI_HATA,
        "str->bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str->boy == 0,
        KAYA_BILDIRI_UYARI,
        "str->boy parametresi degeri 0"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str2.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str2.boy parametresi degeri 0"
    );

    kaya_string_ekle(str, str2);

    va_list liste;
    va_start(liste, str2);
    int arg_sayisi = va_arg(liste, int);
    for(int i = 0; i < arg_sayisi; ++i)
    {
        KayaStringOrnek const diger = va_arg(liste, KayaStringOrnek);
        kaya_string_ekle(str, diger);
    }
    va_end(liste);
}

void kaya_string_coklu_ekle_ks_n(KayaString* str, KayaString const str2, ...)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str2.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str2.boy degeri 0"
    );

    kaya_string_ekle(str, str2);

    va_list liste;
    va_start(liste, str2);
    int arg_sayisi = va_arg(liste, int);
    for(int i = 0; i < arg_sayisi; ++i)
    {
        KayaString const diger = va_arg(liste, KayaString);
        kaya_string_ekle(str, diger);
    }
    va_end(liste);
}

void kaya_string_coklu_ekle_cstr_n(KayaString *const str, char const *const str2, ...)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str->bas == NULL,
        KAYA_BILDIRI_HATA,
        "str->bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2 == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    kaya_string_ekle(str, str2);

    va_list liste;

    va_start(liste, str2);
    int arg_sayisi = va_arg(liste, int);
    for(int i = 0; i < arg_sayisi; ++i)
    {
        char const* const diger = va_arg(liste, char const* const);
        kaya_string_ekle(str, diger);
    }
    va_end(liste);
}

KayaSonuc kaya_string_genislet(
    KayaString* const str, 
    uint32_t const boyut
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    return kaya_esnek_hafiza_gerekli_boyutu_karsila(
        &str->hafiza_parca,
        str->boy + boyut
    );
}

void kaya_string_temizle(KayaString* const str)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    kaya_string_temizle_gelismis(kaya_temel_hafiza_yonetici, str);
}

void kaya_string_temizle_gelismis(
    KayaHafizaYonetici const hafiza_yonetici, 
    KayaString* const str
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    kaya_dizi_temizle_gelismis_fonk(hafiza_yonetici, &str->dizi_bilgi);
    str->boy = 0;
    str->bas = NULL;
}

KayaStringOrnek kaya_ds_dosya_adini_getir_kso(KayaStringOrnek const str)
{
    KayaStringOrnek sonuc = {
        str.bas,
        str.boy
    };

    bool dongu_bitir = false;
    for(char* cur = str.bas + str.boy; cur >= str.bas && !dongu_bitir; cur--)
    {
        if(*cur == '.')
        {
            sonuc.boy = cur - str.bas;
            dongu_bitir = true;
        }
    }

    return sonuc;
}

KayaStringOrnek kaya_ds_dosya_adini_getir_cstr(char *const str)
{
    KayaStringOrnek girdi = {
        .bas = str,
        .boy = strlen(str),
    };
    return kaya_ds_dosya_adini_getir_kso(girdi);
}

KayaStringOrnek kaya_ds_dosya_adini_getir_ks(KayaString const str)
{
    KayaStringOrnek girdi = {
        str.bas,
        str.boy
    };

    return kaya_ds_dosya_adini_getir_kso(girdi);
}

KayaStringOrnek kaya_ds_dosya_uzantisini_getir_ks(KayaString const str)
{
    KayaStringOrnek girdi = {
        .bas = str.bas,
        .boy = str.boy
    };

    return kaya_ds_dosya_uzantisini_getir_kso(girdi);
}

KayaStringOrnek kaya_ds_dosya_uzantisini_getir_kso(KayaStringOrnek const str)
{
    KayaStringOrnek sonuc = {0};

    bool dongu_bitir = false;
    for(char* cur = str.bas + str.boy; cur >= str.bas && !dongu_bitir; cur--)
    {
        if(*cur == '.')
        {
            sonuc.bas = cur + sizeof(char);
            sonuc.boy = (str.bas + str.boy) - sonuc.bas;
            dongu_bitir = true;
        }
    }

    return sonuc;
}

KayaStringOrnek kaya_ds_dosya_uzantisini_getir_cstr(char *const str)
{
    KayaStringOrnek girdi = {
        .bas = str,
        .boy = strlen(str)
    };

    return kaya_ds_dosya_uzantisini_getir_kso(girdi);
}

KayaDosyaIstatistikSonuc kaya_ds_dosya_istatistik_kso(KayaStringOrnek const str)
{
    char dosya_isim[PATH_MAX];

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str.boy parametresi 0!"
    );

    snprintf(dosya_isim, PATH_MAX-1, "%.*s", str.boy, str.bas);
    return kaya_ds_dosya_istatistik_cstr(dosya_isim);
}

KayaDosyaIstatistikSonuc kaya_ds_dosya_istatistik_ks(KayaString const str)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str.boy parametresi 0!"
    );
    
    return kaya_ds_dosya_istatistik_cstr(str.bas);
}

KayaDosyaIstatistikSonuc kaya_ds_dosya_istatistik_cstr(char const* const str)
{
    KayaDosyaIstatistikSonuc ret = {0};

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    int s = stat(str, &ret.sonuc);

    if(s == 0)
    {
        ret.durum = KAYA_DOSYA_ISTATISTIK_SONUC_BASARILI;
    }
    else
    {
        switch (errno)
        {
        case ENOENT :
            ret.durum = KAYA_DOSYA_ISTATISTIK_SONUC_DOSYA_BULUNAMADI;
            break;
        case EINVAL :
            ret.durum = KAYA_DOSYA_ISTATISTIK_SONUC_GECERSIZ_PARAMATRE_2;
            break;
        default:
            ret.durum = KAYA_DOSYA_ISTATISTIK_SONUC_TANIMSIZ;
        }
    }

    return ret;
}

bool kaya_ds_dosya_var_mi_ks(KayaString const str)
{
    KayaStringOrnek girdi = {
        .bas = str.bas,
        .boy = str.boy
    };

    return kaya_ds_dosya_var_mi_kso(girdi);
}

bool kaya_ds_dosya_var_mi_kso(KayaStringOrnek const str)
{
    char dosya_ismi[PATH_MAX];
    snprintf(dosya_ismi, PATH_MAX, "%.*s", str.boy, str.bas);

    return kaya_ds_dosya_var_mi_cstr(dosya_ismi);
}

bool kaya_ds_dosya_var_mi_cstr(char const *const str)
{
    bool sonuc;
    if(0 == access(str, 0))
    {
        sonuc = true;
    }
    else
    {
        sonuc = false;
    }

    return sonuc;
}

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr(
    char *const str,
    char *const str2
)
{
    KayaDosyaIstatistikSonuc dosya1;
    KayaDosyaIstatistikSonuc dosya2;

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2 == NULL,
        KAYA_BILDIRI_HATA,
        "str2 parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        kaya_ds_dosya_var_mi(str) == false,
        KAYA_BILDIRI_UYARI,
        "%s dosyasi bulunamadi!",
        str
    );
    
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        kaya_ds_dosya_var_mi(str2) == false,
        KAYA_BILDIRI_UYARI,
        "%s dosyasi bulunamadi!",
        str2
    );

    dosya1 = kaya_ds_dosya_istatistik(str);
    dosya2 = kaya_ds_dosya_istatistik(str2);

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        KAYA_DOSYA_ISTATISTIK_SONUC_BASARILI != dosya1.durum,
        KAYA_BILDIRI_UYARI,
        "%s dosyasi stati alinamadi!",
        str
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        KAYA_DOSYA_ISTATISTIK_SONUC_BASARILI != dosya2.durum,
        KAYA_BILDIRI_UYARI,
        "%s dosyasi stati alinamadi!",
        str2
    );
    
    return dosya1.sonuc.st_mtime - dosya2.sonuc.st_mtime;
}

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_cstr_kso(
    char *const str,
    KayaStringOrnek const str2
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str2.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str2.boy degeri 0!"
    );

    char kso_son_karakter = str2.bas[str2.boy];
    str2.bas[str2.boy] = 0;

    int64_t fark = kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr(str, str2.bas);

    str2.bas[str2.boy] = kso_son_karakter;
    return fark;
}

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_cstr_ks(
    char *const str,
    KayaString const str2
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str == NULL,
        KAYA_BILDIRI_HATA,
        "str parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str2.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str2.boy degeri 0!"
    );

    return kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr(str, str2.bas);
}

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_kso_cstr(
    KayaStringOrnek const str,
    char *const str2
)
{
    return -kaya_ds_dosya_duzenlenme_zamani_farki_cstr_kso(str2, str);
}

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_kso_kso(
    KayaStringOrnek const str,
    KayaStringOrnek const str2
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str.boy degeri 0!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str2.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str2.boy degeri 0!"
    );

    char str_son_harf = str.bas[str.boy];
    char str2_son_harf = str2.bas[str2.boy];

    str.bas[str.boy] = 0;
    str2.bas[str2.boy] = 0;    

    int64_t fark = kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr(str.bas, str2.bas);

    str2.bas[str2.boy] = str2_son_harf;
    str.bas[str.boy] = str_son_harf;

    return fark;
}

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_kso_ks(
    KayaStringOrnek const str,
    KayaString const str2
)
{
    return kaya_ds_dosya_duzenlenme_zamani_farki_kso_cstr(str, str2.bas);
}

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_ks_cstr(
    KayaString const str,
    char *const str2
)
{
    return -kaya_ds_dosya_duzenlenme_zamani_farki_cstr_ks(str2, str);
}

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_ks_kso(
    KayaString const str,
    KayaStringOrnek const str2
)
{
    return -kaya_ds_dosya_duzenlenme_zamani_farki_kso_ks(str2, str);
}

int64_t kaya_ds_dosya_duzenlenme_zamani_farki_ks_ks(
    KayaString const str,
    KayaString const str2
)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str.boy degeri 0!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.bas == NULL,
        KAYA_BILDIRI_HATA,
        "str2.bas parametresi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        str2.boy == 0,
        KAYA_BILDIRI_UYARI,
        "str2.boy degeri 0!"
    );

    return kaya_ds_dosya_duzenlenme_zamani_farki_cstr_cstr(str.bas, str2.bas);
}

uint32_t kaya_ds_dosya_boyutunu_getir_cstr(char const* dosya_adi)
{
    KayaDosyaIstatistikSonuc dosya_ist;

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        dosya_adi == NULL,
        KAYA_BILDIRI_HATA,
        "dosya_adi NULL olamaz!"
    );

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        kaya_ds_dosya_var_mi(dosya_adi) == false,
        KAYA_BILDIRI_HATA,
        "dosya boyutu alinamadi: %s bulunamadi!", dosya_adi
    );

    dosya_ist = kaya_ds_dosya_istatistik(dosya_adi);

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        dosya_ist.durum != KAYA_DOSYA_ISTATISTIK_SONUC_BASARILI,
        KAYA_BILDIRI_HATA,
        "dosya boyutu alinamadi: %s", kaya_hata_kodundan_hata_mesaji(dosya_ist.durum)
    );

    return dosya_ist.sonuc.st_size;
}

uint32_t kaya_ds_dosya_boyutunu_getir_kso(KayaStringOrnek const dosya_adi)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        dosya_adi.bas == NULL,
        KAYA_BILDIRI_HATA,
        "dosya_adi NULL olamaz!"
    );
    
    char son_karakter = dosya_adi.bas[dosya_adi.boy];
    dosya_adi.bas[dosya_adi.boy] = 0;
    
    uint32_t sonuc = kaya_ds_dosya_boyutunu_getir_cstr(dosya_adi.bas);
    dosya_adi.bas[dosya_adi.boy] = son_karakter;
    return sonuc;
}

uint32_t kaya_ds_dosya_boyutunu_getir_ks(KayaString const dosya_adi)
{
    return kaya_ds_dosya_boyutunu_getir_cstr(dosya_adi.bas);
}

void kaya_ds_dosya_akisindan_oku_buf(KayaDosyaAkis* akis, void* buf, uint64_t maks_boyut)
{
    uint32_t okunan = fread(buf, sizeof(char), maks_boyut - KAYA_NULL_TERMINATOR_BOYUT, akis->dosya);
    akis->okunan += okunan * sizeof(char);
    ((char*)buf)[okunan] = 0;
    if(akis->okunan == akis->dosya_boyut)
    {
        akis->durum = KAYA_DOSYA_AKIS_TAMAMLANDI;
    }
}

void kaya_ds_dosya_akisindan_oku_ks(KayaDosyaAkis* akis, KayaString* buf)
{
    size_t okunan = fread(
        buf->bas + (buf->boy * sizeof(char)),
        sizeof(char),
        buf->hafiza_parca.boy - (buf->boy + KAYA_NULL_TERMINATOR_BOYUT * sizeof(char)),
        akis->dosya
    );

    akis->okunan += okunan;
    buf->boy += okunan;
    buf->bas[buf->boy] = 0;

    if(akis->okunan == akis->dosya_boyut)
    {
        akis->durum = KAYA_DOSYA_AKIS_TAMAMLANDI;
    }
}

void kaya_ds_dosya_akisini_bitir(KayaDosyaAkis const* akis)
{
    fclose(akis->dosya);
    memset((void*)akis, 0, sizeof(*akis));
}

KayaDosyaAkisBaslatmaSonuc kaya_ds_dosya_akisini_baslat_ks(KayaString const dosya_adi)
{
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        dosya_adi.bas == NULL,
        KAYA_BILDIRI_HATA,
        "dosya_adi.bas NULL olamaz!"
    );

    return kaya_ds_dosya_akisini_baslat_cstr(dosya_adi.bas);
}

KayaDosyaAkisBaslatmaSonuc kaya_ds_dosya_akisini_baslat_kso(KayaStringOrnek const dosya_adi)
{
    KayaDosyaAkisBaslatmaSonuc sonuc;
    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        dosya_adi.bas == NULL,
        KAYA_BILDIRI_HATA,
        "dosya_adi.bas NULL olamaz!"
    );

    char son_harf = dosya_adi.bas[dosya_adi.boy];
    dosya_adi.bas[dosya_adi.boy] = 0;
    sonuc = kaya_ds_dosya_akisini_baslat_cstr(dosya_adi.bas);
    dosya_adi.bas[dosya_adi.boy] = son_harf;
    return sonuc;
}

KayaDosyaAkisBaslatmaSonuc kaya_ds_dosya_akisini_baslat_cstr(char const* const dosya_adi)
{
    KayaDosyaAkisBaslatmaSonuc sonuc = {0};

    KAYA_GELISTIRME_KONTROL_ET_BILDIR(
        dosya_adi == NULL,
        KAYA_BILDIRI_HATA,
        "dosya_adi NULL olamaz!"
    );

    if(kaya_ds_dosya_var_mi(dosya_adi))
    {
        sonuc.akis.dosya = fopen(dosya_adi, "rb");

        if(sonuc.akis.dosya != NULL)
        {
            sonuc.akis.dosya_boyut = kaya_ds_dosya_boyutunu_getir(dosya_adi);
            sonuc.akis.durum = KAYA_DOSYA_AKIS_DEVAM_EDIYOR;
        }
        else
        {
            KAYA_GELISTIRME_KONTROL_ET_BILDIR(
                true,
                KAYA_BILDIRI_UYARI,
                "Dosya acilamadi. Hata: %s",
                strerror(errno)
            );

            sonuc.durum = KAYA_DOSYA_AKIS_BASLATMA_SONUC_DOSYA_ACILAMADI;
        }
    }
    else
    {
        sonuc.durum = KAYA_DOSYA_AKIS_BASLATMA_SONUC_DOSYA_BULUNAMADI;
    }

    return sonuc;
}

int kaya_komut_calistir_ks(KayaString const str)
{
    printf("[ KOMUT ] %s\n", str.bas);
    return system(str.bas);
}

int kaya_komut_calistir_kso(KayaString const str)
{
    char son_kar = str.bas[str.boy];
    str.bas[str.boy] = 0;
    printf("[ KOMUT ] %s\n", str.bas);
    int sonuc = system(str.bas);
    str.bas[str.boy] = son_kar;
    return sonuc;
}

int kaya_komut_calistir_cstr(char const *const str)
{
    printf("[ KOMUT ] %s\n", str);
    return system(str);
}

#define kaya_komut_calistir(X) _Generic((X), \
    char*: kaya_komut_calistir_cstr,\
    char const*: kaya_komut_calistir_cstr, \
    KayaString: kaya_komut_calistir_ks, \
    KayaStringOrnek: kaya_komut_calistir_kso \
)(X)

void kaya_bsis_gerekliyse_tekrar_derle_fonk(
    char* cagrilan_dosya_adi,
    int argc,
    char*argv[]
)
{
    KayaString exe_adi = {0};
    kaya_string_hazirla_boyut(&exe_adi, strlen(cagrilan_dosya_adi) + 5);
    kaya_string_ekle(&exe_adi, kaya_ds_dosya_adini_getir(cagrilan_dosya_adi));
    kaya_string_ekle(&exe_adi, ".exe");

    int64_t zaman_farki = kaya_ds_dosya_duzenlenme_zamani_farki(exe_adi, cagrilan_dosya_adi);
    if(zaman_farki < 0)
    {
        // tekrar derle ve calistir
        if(NULL != strstr(argv[0], "kaya_tekrar_derleyici"))
        {
            kaya_bildir(
                KAYA_BILDIRI_BILGI,
                "%s degisti tekrar derleniyor",
                cagrilan_dosya_adi
            );

            KayaString tekrar_derleme_komut = {0};
            kaya_string_hazirla_boyut(
                &tekrar_derleme_komut,
                sizeof("gcc ") + sizeof(__FILE__) + sizeof(" -o ") + exe_adi.boy
            );

            kaya_string_coklu_ekle(
                &tekrar_derleme_komut,
                "gcc ", cagrilan_dosya_adi, " -o ", exe_adi.bas
            );

            if(0 != kaya_komut_calistir(tekrar_derleme_komut))
            {
                kaya_bildir(
                    KAYA_BILDIRI_HATA,
                    "Tekrar derleyici kendi kodunu derleneyemedi hatalari duzelt tekrar dene!"
                );
                exit(1);
            }
            else
            {
                kaya_bildir(
                    KAYA_BILDIRI_BILGI,
                    "Derleme basarili!"
                );
            }

            kaya_komut_calistir("rm kaya_tekrar_derleyici.exe");

            KayaCstrDizi arguman_listesi = {0};
            kaya_dizi_hazirla_boyut(&arguman_listesi, argc + 1);

            kaya_dizi_ekle(&arguman_listesi, exe_adi.bas);
            for(int i = 1; i < argc; ++i)
            {
                kaya_dizi_ekle(&arguman_listesi, argv[i]);
            }
            kaya_dizi_ekle(&arguman_listesi, NULL);

            printf("[ KOMUT ] execv %s\n", exe_adi.bas);
            execv(exe_adi.bas, arguman_listesi.bas);
            kaya_bildir(
                KAYA_BILDIRI_HATA,
                "Tekrar derleyici calistirilamadi: %s\n", 
                strerror(errno)
            );
            exit(1);
        }
        else
        {
            KayaString komut = {0};
            kaya_string_hazirla(&komut);
            kaya_string_coklu_ekle(&komut, "cp ", argv[0], " kaya_tekrar_derleyici.exe");
            kaya_komut_calistir(komut);

            KayaCstrDizi arguman_listesi = {0};
            kaya_dizi_hazirla_boyut(&arguman_listesi, argc + 1);

            kaya_dizi_ekle(&arguman_listesi, "kaya_tekrar_derleyici.exe");
            for(int i = 1; i < argc; ++i)
            {
                kaya_dizi_ekle(&arguman_listesi, argv[i]);
            }
            kaya_dizi_ekle(&arguman_listesi, NULL);

            printf("[ KOMUT ] execv kaya_tekrar_derleyici.exe\n");
            execv("kaya_tekrar_derleyici.exe", arguman_listesi.bas);
            kaya_bildir(
                KAYA_BILDIRI_HATA,
                "Tekrar derleyici calistirilamadi: %s\n", 
                strerror(errno)
            );
            exit(1);
        }
    }
}

void kaya_bsis_string_ayarla_cstr(KayaString* hedef, char const* const str)
{
    kaya_string_hazirla_boyut(hedef, strlen(str));
    kaya_string_ekle(hedef, str);
}

void kaya_bsis_string_ayarla_kso(KayaString* hedef, KayaStringOrnek const str)
{
    kaya_string_hazirla_boyut(hedef, str.boy);
    kaya_string_ekle(hedef, str);
}

void kaya_bsis_string_ayarla_ks(KayaString* hedef, KayaString const str)
{
    kaya_string_hazirla_boyut(hedef, str.boy);
    kaya_string_ekle(hedef, str);
}

void kaya_bsis_string_dizi_ayarla_cstr(KayaStringDizi* hedef, char** str_dizi, uint32_t adet)
{
    kaya_dizi_hazirla_boyut(hedef, adet);
    for(uint32_t i = 0; i < adet; ++i)
    {
        KayaString gecici = {0};
        kaya_string_hazirla_boyut(&gecici, strlen(str_dizi[i]));
        kaya_string_ekle(&gecici, str_dizi[i]);
        kaya_dizi_ekle(hedef, gecici);
    }
}

void kaya_bsis_string_dizi_ayarla_kso(KayaStringDizi* hedef, KayaStringOrnekDizi str_dizi, uint32_t adet)
{
    kaya_dizi_hazirla_boyut(hedef, adet);
    for(uint32_t i = 0; i < adet; ++i)
    {
        KayaString gecici = {0};
        kaya_string_hazirla_boyut(&gecici, str_dizi.bas[i].boy);
        kaya_string_ekle(&gecici, str_dizi.bas[i]);
        kaya_dizi_ekle(hedef, gecici);
    }
}

void kaya_bsis_string_dizi_ayarla_ks(KayaStringDizi* hedef, KayaStringDizi str_dizi, uint32_t adet)
{
    kaya_dizi_hazirla_boyut(hedef, adet);
    for(uint32_t i = 0; i < adet; ++i)
    {
        KayaString gecici = {0};
        kaya_string_hazirla_boyut(&gecici, str_dizi.bas[i].boy);
        kaya_string_ekle(&gecici, str_dizi.bas[i]);
        kaya_dizi_ekle(hedef, gecici);
    }
}

void kaya_bsis_komutlari_uret(KayaBsisHedef* hedef)
{
    // +1 son olarak uretilecek exe icin
    kaya_dizi_hazirla_boyut(&hedef->komutlar, hedef->srcs.boy + 1);

    KayaString son_komut = {0};
    kaya_string_hazirla(&son_komut);

    kaya_string_ekle(&son_komut, hedef->derleyici);
    kaya_string_coklu_ekle(
        &son_komut, 
        " -o ", hedef->cikti_klasoru.bas, KAYA_DS_YOL_AYRAC, hedef->hedef_adi.bas, " "
    );

    for(uint32_t i = 0; i < hedef->srcs.boy; ++i)
    {
        KayaString komut = {0};
        kaya_string_hazirla(&komut);

        kaya_string_ekle(&komut, hedef->derleyici);
        kaya_string_ekle(&komut, " -c ");
        kaya_string_ekle(&komut, hedef->srcs.bas[i]);
        kaya_string_ekle(&komut, " -o ");

        KayaString cikti_dosya_adi = {0};
        kaya_string_hazirla_boyut(&cikti_dosya_adi, hedef->srcs.bas[i].boy + 2); // + 2 ".o"

        kaya_string_ekle(&cikti_dosya_adi, hedef->cikti_klasoru);
        kaya_string_ekle(&cikti_dosya_adi, KAYA_DS_YOL_AYRAC);
        kaya_string_ekle(&cikti_dosya_adi, hedef->srcs.bas[i]);
        kaya_string_ekle(&cikti_dosya_adi, ".o ");

        kaya_string_ekle(&komut, cikti_dosya_adi);
        kaya_string_ekle(&son_komut, cikti_dosya_adi);

        kaya_string_temizle(&cikti_dosya_adi);

        for(uint32_t i = 0; i < hedef->cflags.boy; ++i)
        {
            kaya_string_genislet(&komut, hedef->cflags.bas[i].boy + 1);
            kaya_string_coklu_ekle(&komut, " ", hedef->cflags.bas[i].bas);
        }

        kaya_dizi_ekle(&hedef->komutlar, komut);
    }

    for(uint32_t i = 0; i < hedef->lflags.boy; ++i)
    {
        kaya_string_genislet(&son_komut, hedef->lflags.bas[i].boy + 1);
        kaya_string_ekle(&son_komut, hedef->lflags.bas[i]);
        kaya_string_ekle(&son_komut, " ");
    }

    kaya_dizi_ekle(&hedef->komutlar, son_komut);
}

#endif // KAYA_TANIMLAR
#endif // KAYA_LIB_H